package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Spotlight struct {
	BaseUrl  string
	Username string
	Password string
	SolrUrl  string
}

type FieldType struct {
	Label     string `json:label`
	Weight    string `json:weight`
	List      bool   `json:list`
	Gallery   bool   `json:gallery`
	Slideshow bool   `json:slideshow`
	Show      bool   `json:show`
	Enabled   bool   `json:enabled`
}

type Fields map[string]FieldType

func (s *Spotlight) fields() map[string]FieldType {
	client := &http.Client{}
	req, err := http.NewRequest("GET", s.BaseUrl+"/metadata.json", nil)
	req.SetBasicAuth(s.Username, s.Password)
	resp, err := client.Do(req)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	var data Fields
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Printf("%T\n%s\n%#v\n", err, err, err)
	}
	return data
}
