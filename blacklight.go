package main

import ()

// Record is a simple container for a hashtable of JSON data.
type Record map[string]interface{}
