![SharedShelf](http://catalog.sharedshelf.artstor.org/images/sharedshelflogo.gif)

# Shelfight

Shelfight is an experimental service to allow the Shared Shelf publisher to publish records into a SOLR index as used by **Blacklight** and **Spotlight**.  Functionality is quite limited at the moment but it serves as a proof of concept.


## Current functionality

The service provides three services (API calls) to Shared Shelf:

* A schema call that allows the a publication target to be configured for publishing into the SOLR index.
	  
* A publication call that inserts a Shared Shelf record in to the SOLR index.
	
* A deletion call that removes a Shared Shelf record from the SOLR index.

	###### Note: Currently the records published from Shared Shelf use the SSID for indexing.

On startup a JSON based configuration file is used to configure the service and list the fields available for mapping.  _See the examples directory for a sample configuration file._

## Current limitations

The service only provides for using Shared Shelf API calls for retrieving thumbnails and images.  There are placeholders in the configuration file for transferring the image with the metadata but these are not yet implemented.

## Building

The service is written in Go 1.5.  It will most likely compile in earlier versions, but 1.5 is the earliest supported version. 