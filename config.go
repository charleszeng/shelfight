package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// Field is a mapping aid for listing fields available in the
// SOLR index.  The publisher can target the configured fields.
type Field struct {
	Label      string           `json:"label"`
	MultiValue bool             `json:"multiValue"`
	Static     *json.RawMessage `json:"static,omitempty"`
}

// Config holds all the configuration information read from the
// configuration file.
type Config struct {
	Name             string           `json:"name"`
	Url              string           `json:"url"`
	SolrUrl          string           `json:"solrUrl"`
	Fields           map[string]Field `json:"fields,omitempty"`
	Thumbnail        bool             `json:"publishThumbnail"`
	LargeImage       bool             `json:"publishLargeImage"`
	baseImageUrl     string
	imageStoragePath string
    Port             int32            `json:"listenPort"`
}

// Read our configuration file.
//
// If the path isn't specified, use PWD.
func loadConfig(path string) Config {
	if len(path) == 0 {
		path = "./shelfight.cfg"
	}
	file, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Printf("Can't read configuration from %s\n", path)
		os.Exit(1)
	}
	var c Config
	err = json.Unmarshal(file, &c)
	if err != nil {
		fmt.Printf("Parsing exception reading configuration file: %s", err)
		os.Exit(2)
	}
	return c
}

func (c *Config) updateURL() string {
	return c.SolrUrl + "/blacklight-core/update?commit=true"
}

func (c *Config) schemaURL() string {
	return c.SolrUrl + "/blacklight-core/schema/json"
}
